# Adapter for FATHMM-DB

## Description

Flask App that requests FATHMM MySQL database and can itself be requested via localhost.

To run the application in a docker container, type in the following command in your
command line:

docker-compose up

Usage: localhost:8080/toFATHMM?variant=[UniProt-ID]%20[protein description of
variant]&weights=[Unweighted, Inherited, Cancer]

--> variant parameter is mandatory

Example input: localhost:8000/toFATHMM?variant=P04637%20R282W&weights=Cancer

Example output: {R282W:
                    {"Domain-Phenotype Association":"",
                    "Prediction":"CANCER",
                    "Protein ID":"P04637",
                    "Score":"-9.73",
                    "Warning":"",
                    "dbSNP ID":""
                    }
                }
