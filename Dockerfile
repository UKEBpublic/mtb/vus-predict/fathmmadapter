FROM python:3.6-alpine

RUN     apk update
RUN     apk add --no-cache --virtual .build-deps build-base libffi-dev openssl-dev musl-dev curl

WORKDIR /app
ADD ./requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt
COPY . /app

RUN mkdir /app/data
RUN curl -X GET -H "PRIVATE-TOKEN: YqQhg-yv_3vG7t9ubDf3" -L "http://gitlab.gwdg.de/api/v4/projects/12007/repository/branches/master" -o /app/data/current_commit.json

CMD ["python", "fathmm.py"]
