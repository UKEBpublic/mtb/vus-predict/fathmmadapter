#!/usr/bin/python -u

import re
import math
import argparse
import configparser as ConfigParser
import requests
import json
from flask import Flask
from flask_restful import reqparse
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint

import pymysql
pymysql.install_as_MySQLdb()

from dbutils.persistent_db import PersistentDB
#

app = Flask(__name__)
CORS(app)

### Swagger config ###
SWAGGER_URL = '/FATHMM/v1/doc'
API_URL = '/static/openapi.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "FATHMM-API"
    }
)
app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)


import file_loader
template = file_loader.load_template('template/template.json')

def map_position(domain, substitution):
    """
    """
    
    if int(substitution[1:-1]) < int(domain['seq_begin']) or \
       int(substitution[1:-1]) > int(domain['seq_end']):
           return None
    
    x = int(domain['seq_begin']) - 1 
    y = int(domain['hmm_begin']) - 1 
    
    for residue in list(domain['align']):
        if residue.isupper() or residue.islower():
            # upper-case (match)/lower-case (insertion) characters correspond 
            # to a sequence residue
            x += 1
        if residue.isupper() or residue == "-":
            # upper-case (match)/gap (deletion) characters correspond to
            # a HMM position
            y += 1
        if x == int(substitution[1:-1]):
            if residue.isupper():
                return str(y)
            # unmapped residue (i.e. insertion)
            return None
    return None

#
def fetch_phenotype_prediction(Facade, Phenotype, Arg):
    """
    """
    
    Phenotypes = ""
    
    if not Arg.phenotypes:
        return Phenotypes
    
    for x in sorted(Facade, key=lambda x:x['information'], reverse=True):
        # use the most informative HMM
        if x['accession']:
            dbCursor.execute("select * from PHENOTYPES where accession='" + x['accession'] + "' and type='" + Phenotype + "' and origin=1 order by score")
            Phenotypes = "|".join([ x['description'] for x in dbCursor.fetchall() ])
            
            if Phenotypes:
                break
    
    return Phenotypes

#
def Process(Protein, Substitution, Arg, Weights=None, Cutoff=None, Phenotype=None):
    """
    """
    
    Processed = {
                 'Prediction' : "",
                 'Score':       "",       
                 'Phenotype':   "",
                 'HMM':         "",
                 'Description': "",
                 'Position':    "",
                 'W':           "",
                 'M':           "",
                 'D':           "",
                 'O':           "",
                 'Warning':     "",
                }
    
    # fetch pre-computed sequence record
    dbCursor.execute("select a.* from SEQUENCE a, PROTEIN b where a.id=b.id and b.name='" + Protein + "'")
    Sequence = dbCursor.fetchone()
    
    if not Sequence:
        Processed['Warning'] = "No Sequence Record Found"
        
        return Processed
    
    # authenticate protein/substitution
    Warning     = None
    if not Warning and not re.compile("^[ARNDCEQGHILKMFPSTWYV]\d+[ARNDCEQGHILKMFPSTWYV]$", re.IGNORECASE).match(Substitution):
        Warning = "Invalid Substitution Format"
    if not Warning and int(Substitution[1:-1]) > len(Sequence['sequence']):
        Warning = "Invalid Substitution Position"
    if not Warning and not Substitution[0] == Sequence['sequence'][int(Substitution[1:-1]) - 1]:
        Warning = "Inconsistent Wild-Type Residue (Expected '" + Sequence['sequence'][int(Substitution[1:-1]) - 1] + "')"
    if not Warning and Substitution[0] == Substitution[-1]:
        Warning = "Synonymous Mutation"
        
    if Warning:
        Processed['Warning'] = Warning
        
        return Processed
    
    # fetch pre-computed domain assignment(s)
    dbCursor.execute("select * from DOMAINS where id='" + str(Sequence['id']) + "' and " + Substitution[1:-1] + " between seq_begin and seq_end order by score")
    Domain = dbCursor.fetchall()
    
    Facade = []
    
    for x in Domain:
        residue = map_position(x, Substitution)
        
        if residue:
            dbCursor.execute("select a.*, b.* from PROBABILITIES a, LIBRARY b where a.id=b.id and a.id='" + str(x['hmm']) + "' and a.position='" + residue + "'")
            Prob = dbCursor.fetchone()
            
            if Prob:
                Facade.append(Prob)
    
    #
    
    # fetch phenotype association(s)
    if Phenotype:
        Processed['Phenotype'] = fetch_phenotype_prediction(Facade, Phenotype, Arg)
    
    # derive/return a prediction ...
    if not Weights or Weights == "UNWEIGHTED":
        # append non-domain-based/sequence conservation to the Facade
        dbCursor.execute("select a.*, b.*  from PROBABILITIES a, LIBRARY b where a.id=b.id and a.id='" + str(Sequence['id']) + "' and a.position='" + Substitution[1:-1] + "'")
        Prob = dbCursor.fetchone()
        
        if Prob:
            Facade.append(Prob)
        
        for x in sorted(Facade, key=lambda x:x['information'], reverse=True):
            try:
                Processed['HMM']         = x['id']
                Processed['Description'] = x['description']
                Processed['Position']    = x['position']
                Processed['W']           = x[Substitution[0]]
                Processed['M']           = x[Substitution[-1]]
                Processed['D']           = ""
                Processed['O']           = ""
                
                Processed['Score']       = "%.2f" % math.log((Processed['M'] / (1.0 - Processed['M'])) / (Processed['W'] / (1.0 - Processed['W'])), 2)

                Processed['Prediction']  = ""                
                if Cutoff:
                    if float(Processed['Score']) <= Cutoff: Processed['Prediction'] = "DAMAGING"
                    if float(Processed['Score']) >  Cutoff: Processed['Prediction'] = "TOLERATED"
                
                return Processed
            except Exception as e:
                # skip the rare occasion(s) when probabilities are zero
                pass
    else:
        # prioritize domain-based predictions
        for x in sorted(Facade, key=lambda x:x['information'], reverse=True):
            try:
                dbCursor.execute("select * from WEIGHTS where id='" + x['id'] + "' and type='" + Weights + "'")
                w = \
                    dbCursor.fetchone()
                
                if w:
                    Processed['HMM']         = x['id']
                    Processed['Description'] = x['description']
                    Processed['Position']    = x['position']
                    Processed['W']           = x[Substitution[0]]
                    Processed['M']           = x[Substitution[-1]]
                    Processed['D']           = w['disease'] + 1.0
                    Processed['D']           = w['disease'] + 1.0
                    Processed['O']           = w['other'] + 1.0
                    
                    Processed['Score']       = "%.2f" % math.log(((1.0 - Processed['W']) * Processed['O']) / ((1.0 - Processed['M']) * Processed['D']), 2)

                    Processed['Prediction']  = ""                
                    if Cutoff:
                        if Weights == "INHERITED":
                            if float(Processed['Score']) <= Cutoff: Processed['Prediction'] = "DAMAGING"
                            if float(Processed['Score']) >  Cutoff: Processed['Prediction'] = "TOLERATED"
                        
                        if Weights == "CANCER":
                            if float(Processed['Score']) <= Cutoff: Processed['Prediction'] = "CANCER"
                            if float(Processed['Score']) >  Cutoff: Processed['Prediction'] = "PASSENGER/OTHER"
                    
                    return Processed
            except Exception as e:
                # skip the rare occasion(s) when probabilities are zero
                pass
        
        # no domain-based prediction, use a non-domain-based/sequence conservation prediction
        dbCursor.execute("select a.*, b.*  from PROBABILITIES a, LIBRARY b where a.id=b.id and a.id='" + str(Sequence['id']) + "' and a.position='" + Substitution[1:-1] + "'")
        Facade = dbCursor.fetchone()
        
        if Facade:
            try:
                dbCursor.execute("select * from WEIGHTS where id='" + Facade['id'] + "' and type='" + Weights + "'")
                w = \
                    dbCursor.fetchone()
                
                if w:
                    Processed['HMM']         = Facade['id']
                    Processed['Description'] = Facade['description']
                    Processed['Position']    = Facade['position']
                    Processed['W']           = Facade[Substitution[0]]
                    Processed['M']           = Facade[Substitution[-1]]
                    Processed['D']           = w['disease'] + 1.0
                    Processed['O']           = w['other'] + 1.0
                    
                    Processed['Score']       = "%.2f" % math.log(((1.0 - Processed['W']) * Processed['O']) / ((1.0 - Processed['M']) * Processed['D']), 2)

                    Processed['Prediction']  = ""                
                    if Cutoff:
                        if Weights == "INHERITED":
                            if float(Processed['Score']) <= Cutoff: Processed['Prediction'] = "DAMAGING"
                            if float(Processed['Score']) >  Cutoff: Processed['Prediction'] = "TOLERATED"
                        
                        if Weights == "CANCER":
                            if float(Processed['Score']) <= Cutoff: Processed['Prediction'] = "CANCER"
                            if float(Processed['Score']) >  Cutoff: Processed['Prediction'] = "PASSENGER/OTHER"
                    
                    return Processed
            except Exception as e:
                # skip the rare occasion(s) when probabilities are zero
                raise        

    return None

 # Flask application to read in and process requested protein alterations
 # example: /toFATHMM?variant=P04637 R282W&weights=Cancer
@app.route('/FATHMM/v1/toFATHMM') 
def requestFATHMM():

    # parse program arguments
    parser = reqparse.RequestParser()

    parser.add_argument(
        "help",
        action="help",
        help=argparse.SUPPRESS
    )

    parser.add_argument(
        'uniprotname',
        dest='uniprotname',
        required=True,
        type=str,
        help='usage: uniprotname=P04637'
    )

    parser.add_argument(
        'variant',
        dest='variant',
        required=True,
        type=str,
        help='usage: variant=R282W'
    )

    parser.add_argument(
        'weights',
        dest='weights',
        default='INHERITED',
        type=str,
        help='usage: weights=[WEIGHTED, INHERITED, CANCER]'
    )
    parser.add_argument(
        'threshold',
        dest='threshold',
        default=None,
        type=float,
        help="usage: threshold=0.75"
    )
    parser.add_argument(
        'phenotypes',
        dest='phenotypes',
        default="",
        type=str,
        help="usage: phenotypes=[DO, GO, HP, MP, WP, YP, FP, FA, ZA, AP, KW]"
    )

    Arg = parser.parse_args()

    if Arg['weights']:
        # authenticate prediction weights
        dbCursor.execute("select distinct type from WEIGHTS")

        if not Arg['weights'].upper() in [ x['type'] for x in dbCursor.fetchall() ] + [ "UNWEIGHTED" ]:
            return ("argument weights: invalid option: '{0}'".format(Arg['weights']))

        if Arg.threshold == None:
            # initialize prediction threshold
            if Arg['weights'].upper() == "UNWEIGHTED": Arg.threshold = -3.00
            if Arg['weights'].upper() == "INHERITED":  Arg.threshold = -1.50
            if Arg['weights'].upper() == "CANCER":     Arg.threshold = -8.30

    if Arg['phenotypes']:
        # authenticate prediction phenotype
        dbCursor.execute("select distinct type from PHENOTYPES")

        if not Arg['phenotypes'].upper() in [ x['type'] for x in dbCursor.fetchall() ]:
            return ("argument phenotypes: invalid option: '{0}'".format(Arg['phenotypes']))

    # create dictionary to store substitution name with provided FATHMM information
    substitution_info = {}

    # create dictionary with FATHMM information to substitution
    results = {}

    idx = 1
    # print(Arg['variant'])
    # for record in Arg['variant']:
    uniprotname = Arg['uniprotname'].strip()
    variant = Arg['variant'].strip()
    record = f"{uniprotname} {variant}"

    if record and not record.startswith("#"):
        try:
            if re.compile("^rs\d+$", re.IGNORECASE).match(record):
                dbCursor.execute("select distinct * from VARIANTS where id='" + record + "'")
                dbRecords = dbCursor.fetchall()

                if not dbRecords:
                    # results["#"] = str(idx)
                    # results["dbSNP ID"] = record
                    results["Warning"] = "No dbSNP Mapping(s)"

                    substitution_info[record] = results

                    idx += 1
                    # continue

                for x in dbRecords:
                    dbSNP        = x['id']
                    Protein      = x['protein']
                    Substitution = x['substitution']

                    Prediction = \
                        Process(Protein, Substitution, Arg, Weights=Arg['weights'].upper(), Cutoff=Arg['threshold'], Phenotype=Arg['phenotypes'].upper())

                    if not Prediction:
                        # results["#"] = str(idx)
                        # results["dbSNP ID"] = dbSNP
                        # results["Protein ID"] = Protein
                        # results["Substitution"] = Substitution
                        results["Warning"] = "No Prediction Available"

                        substitution_info[Substitution] = results

                        idx += 1
                        continue

                    # results["#"] = str(idx)
                    # results["dbSNP ID"] = dbSNP
                    # results["Protein ID"] = Protein
                    # results["Substitution"] = Substitution
                    results["Prediction"] = str(Prediction['Prediction'])
                    results["Score"] = str(Prediction['Score'])
                    # results["Domain-Phenotype Association"] = str(Prediction['Phenotype'])
                    results["Warning"] = str(Prediction['Warning'])
                    # results["HMM ID"] = str(Prediction['HMM'])
                    # results["HMM Description"] = str(Prediction['Description'])
                    # results["HMM Pos."] = str(Prediction['Position'])
                    # results["HMM Prob. W."] = str(Prediction['W'])
                    # results["HMM Prob. M."] = str(Prediction['M'])
                    # results["HMM Weights D."] = str(Prediction['D'])
                    # results["HMM Weights O."] = str(Prediction['O'])

                    substitution_info[Substitution] = results

                    idx += 1
                    continue

            else:
                dbSNP         = ""
                Protein       = record.upper().split()[0]

                for Substitution in [ x.strip() for x in record.upper().split()[1].split(",") ]:
                    Prediction = \
                        Process(Protein, Substitution, Arg, Weights=Arg['weights'].upper(), Cutoff=Arg['threshold'], Phenotype=Arg['phenotypes'].upper())

                    if not Prediction:
                        # results["#"] = str(idx)
                        # results["dbSNP ID"] = dbSNP
                        # results["Protein ID"] = Protein
                        # results["Substitution"] = Substitution
                        results["Warning"] = "No Prediction Available"

                        substitution_info[Substitution] = results

                        idx += 1
                        continue

                    # results["#"] = str(idx)
                    # results["dbSNP ID"] = dbSNP
                    # results["Protein ID"] = Protein
                    # results["Substitution"] = Substitution
                    results["Prediction"] = str(Prediction['Prediction'])
                    results["Score"] = str(Prediction['Score'])
                    # results["Domain-Phenotype Association"] = str(Prediction['Phenotype'])
                    results["Warning"] = str(Prediction['Warning'])
                    # results["HMM ID"] = str(Prediction['HMM'])
                    # results["HMM Description"] = str(Prediction['Description'])
                    # results["HMM Pos."] = str(Prediction['Position'])
                    # results["HMM Prob. W."] = str(Prediction['W'])
                    # results["HMM Prob. M."] = str(Prediction['M'])
                    # results["HMM Weights D."] = str(Prediction['D'])
                    # results["HMM Weights O."] = str(Prediction['O'])

                    substitution_info[Substitution] = results

                    idx += 1
                    continue

        except Exception as e:
            results["#"] = str(idx)
            results["Warning"] = "An Error Occured While Processing The Record: " + record

            substitution_info[record] = results

    return substitution_info

@app.route('/FATHMM/v1/info') 
def getMetadata():
    headers = {'PRIVATE-TOKEN': 'YqQhg-yv_3vG7t9ubDf3'}
    metadata = requests.get('http://gitlab.gwdg.de/api/v4/projects/12007/repository/branches/master', headers=headers).json()
    meta_out = {"title": 'FATHMMdb Adapter'}
    meta_out["newest_commit_id"] = metadata["commit"]["short_id"]
    meta_out["newest_commit_message"] = metadata["commit"]["title"]
    meta_out["newest_commit_date"] = metadata["commit"]["authored_date"].split("T")[0]
    meta_out["data"] = {'fathmmdb': {'version': '2.3', 'date': '2021-02-16'}}
    meta_out["current_commit_id"], meta_out["current_commit_date"] = extractCommitData()
    return meta_out



def connectDB():
	Config = ConfigParser.ConfigParser()
	Config.read("./config.ini")
	print('Connect to MYSQL')
	dbCursor = PersistentDB(
		creator=pymysql,
        	host=str(Config.get("DATABASE", "HOST")),
        	port=int(Config.get("DATABASE", "PORT")),
        	user=str(Config.get("DATABASE", "USER")),
        	password=str(Config.get("DATABASE", "PASSWD")),
        	database=str(Config.get("DATABASE", "DB")),
		autocommit=True,
		cursorclass=pymysql.cursors.DictCursor)
	print('Connected successfully')
	
	return dbCursor

def extractCommitData():
    with open("/app/data/current_commit.json", "r") as cf:
        curl_output = cf.readlines()
        commit_info = json.loads(curl_output[0])
        return commit_info["commit"]["short_id"], commit_info["commit"]["authored_date"].split("T")[0]

if __name__ == '__main__':

    # parser configuration
    dbCursor = connectDB().connection().cursor()

    app.run(host='0.0.0.0', port='9004')
